var _ = require('lodash');
var MessageModel = require('../models/chat_message.model');


async function getMessage(req, res, next) {
    console.log(JSON.stringify(req.body));
    try {
      const DataObj = _.cloneDeep(req.body);
      console.log(DataObj.sender_id,DataObj.reciver_id);
      const messageData = await MessageModel.getChatDataOfParticularId(DataObj.sender_id, DataObj.reciver_id);
      res.json({status:1, message: 'Data are fetched',data: { messageData: messageData }});
    } catch (err) {
      console.log(err);
      err.placeMessage = 'Fetch the message';
      next(err);
    }
}

module.exports = {getMessage}