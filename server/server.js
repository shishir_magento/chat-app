const path = require('path');
const http = require('http');
const express = require('express');
var bodyParser = require('body-parser');
const {Users} = require('./utils/user');
const SocketIO = require('socket.io')
const publicPath = path.join(__dirname, '../public');
var app = express();
var port = process.env.PORT || 3000;
var server = http.createServer(app);
var io  = SocketIO(server);
var users = new Users();
var messageModel = require('../models/chat_message.model');
var mess = require('../routes/mess.route');
const {isRealString} = require('./utils/validation');

app.use(bodyParser.json());
app.use(mess);
app.set('view engine', 'ejs');
app.use(express.static(publicPath));
app.set('views', path.join(__dirname, '../public'));
app.set('view engine', 'ejs');
var socket = require('socket.io-client')('http://localhost');
socket.on('connect', function(){
    console.log('angular');
  socket.on('event', function(data){});
  socket.on('disconnect', function(){});
});
app.get('/', function(req, res) {
    var mainId =  req._parsedOriginalUrl.query.substring(3);
    messageModel.getUserForSender(mainId).then(result => {
        messageModel.getChatDataOfParticularId(14,1).then(resultmsg => {
            res.render('main.ejs', {senderUser:result, MessageData:resultmsg
            });
        });
        
    });
});
var name ; 
io.on('connection',(socket)=>{
    socket.on('join',(data,callback)=>{
        if(!isRealString(data.id)){
            return callback('id is reuired');
        }
        users.removeUser(data.id);
        messageModel.getUserInfoById(data.id).then(result => {
            var name = result[0].user_first_name+' '+ result[0].user_last_name;
           // users.addUser(socket.id,data.id,name);
        });
        
       // users.addUser(socket.id,data.id,name);
       users.addUser(socket.id,data.id,data.id);
        io.emit('updateUserList',users);
        callback();
        

    });
    
    socket.on('createMeassage', (message, callback)=>{
        var user = users.getUser(socket.id);
        if(user){
            console.log('message.to',message.to);
            io.to(message.to).emit('newMessage',message);
        }
        console.log(user);
       
        var post = { 
            chat_type:'text',
            chat_sender_id:message.from,
            chat_parent_thread:0,
            chat_receiver_id:message.reciver_id,
            chat_image:'',
            chat_post_id:0,
            chat_file:'mm',
            chat_sent_at:message.createdAt,
            chat_read_at:'cvcv',
            chat_status:1,
            chat_message:message.message
        }
        messageModel.saveMessage(post);
        callback('This message from server');


    });
    socket.on('createCurentLocation', (message)=>{
        
        socket.broadcast.emit('newMessage',{message:message,url:'https://maps.google.com/maps?q='+message.latitude+","+message.longitude});
    });

    socket.on('disconnect', ()=>{
        var user = users.removeUser(socket.id);
        if(user){
            io.emit('updateUserList',users);
        }
        console.log('Dissconnect to server');
    }); 
});
server.listen(port,()=>{
    console.log('Port is On '+port);
});