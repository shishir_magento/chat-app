const Joi = require('joi');

// require and configure dotenv, will load vars in .env in PROCESS.ENV
require('dotenv').config();

// define validation for all the env vars
const envVarsSchema = Joi.object({ 
  DB_HOST: Joi.string().required()
    .description('DB host url'),
  DB_USERNAME: Joi.string().required()
    .description('DB user'),
  DB_PASSWORD: Joi.string()
    .description('DB user password'),
  DB_DATABASE: Joi.string().required()
    .description('DB database'),
}).unknown()
  .required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  mysql: {
    host: envVars.DB_HOST,
    username: envVars.DB_USERNAME,
    password: envVars.DB_PASSWORD,
    database: envVars.DB_DATABASE,
  }
};
module.exports = config;
