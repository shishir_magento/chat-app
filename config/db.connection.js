var mysql      = require('mysql');
var config = require('./config');
const { pick } = require('lodash');
const dbDetails = pick(config.mysql, ['host','username', 'password', 'database']);
var connection = mysql.createConnection({
  host     : dbDetails.host,
  user     : dbDetails.username,
  password : dbDetails.password,
  database : dbDetails.database
});
 
connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }
 
  console.log('connected as id ' + connection.threadId);
});

module.exports = connection;