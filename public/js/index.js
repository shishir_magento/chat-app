var socket = io();
socket.on('connect',function(){
    console.log('connected');
    
});



socket.on('disconnect', function(){
    console.log('Dissconnect to server');
});
// socket.on('newMessage', function(message){
//     console.log('new message');
//     var formattedTime = moment(message.created).format('h:mm a');
//     if(message.message.latitude && message.message.longitude){
//         $('.msg_history').append('<div class="incoming_msg"><div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div><div class="received_msg"><div class="received_withd_msg"><p><a href = '+message.url+'>My current Location</a></p><span class="time_date"> '+formattedTime+'|'+moment(message.created).format('MMM')+' '+moment(message.created).format('D')+ ','+ moment(message.created).startOf('minute').fromNow()+'</span></div></div></div>');
//         scrollToBottom();
//     } else{
//         $('.msg_history').append('<div class="incoming_msg"><div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div><div class="received_msg"><div class="received_withd_msg"><p>'+message.message+'</p><span class="time_date"> '+formattedTime+'   | '+moment(message.created).format('MMM')+' '+moment(message.created).format('D')+ ','+ moment(message.created).startOf('minute').fromNow()+'</span></div></div></div>');
//         scrollToBottom();
//     }
    
// });

socket.on('updateUserList', function(user){
    console.log(user);
    

}) 

$(function () {
    var sendLocationBUtton = $('#send-location')
    sendLocationBUtton.on('click', function(){
        if(!navigator.geolocation){
            return alert("Geolocation is not support by broser");
        }
        navigator.geolocation.getCurrentPosition(function(position){
            console.log('start');
            socket.emit('createCurentLocation',{latitude:position.coords.latitude,
                longitude:position.coords.longitude
            });
            $('.msg_history').append('<div class="outgoing_msg tempchat"><div class="sent_msg"><p><a href=https://maps.google.com/maps?q='+position.coords.latitude+','+position.coords.longitude+'>My current Location</a></p><span class="time_date"> '+moment().format('h:mm a')+'   | '+moment().format('MMM') +' ' + moment().date() +'</span> </div></div>');
            scrollToBottom();
        },function(){
            alert('Unable to fetch location');
        });
    });
    function GetURLParameter(sParam)
    {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam)
            {
                return sParameterName[1];
            }
        }
    }
    socket.emit('join',{'id':GetURLParameter('id') }, function(err){
        if(err){
         $('#socket_id').val(err);
        } else{
          
          
        }
    });
    $(".open").on("click", function(){
        $(".popup-overlay, .popup-content").addClass("active");
      });
      
      //removes the "active" class to .popup and .popup-content when the "Close" button is clicked 
      $(".close, .popup-overlay").on("click", function(){
        $(".popup-overlay, .popup-content").removeClass("active");
    });

    $(document).on("click", ".chat_list", function(){
        var id = $(this).attr('socket_id');
        var reciverId = $(this).attr('id');
        $('#socket_id').val(id);
        $('#reciver_id').val(reciverId);
    });

    $(document).on("click", ".emojionearea", function(){
       var img  = $('.emojionearea-editor img').attr('src');
       img = "<img alt='' class='emojioneemoji' src='"+img+"'></img>";
       $('.emoji_input').append(img);
       $('.emojionearea-editor img').remove();

    });

    $(document).on("click", "#offline", function(){
        $('#oll').hide();
        $('.off_user').show();
    });
    $(document).on("click", "#online", function(){
        $('.off_user').hide();
        $('#oll').show();
    });
    $(document).ready(function(){
        $('#oll').hide();
        $('.off_user').hide();
        
        $("#example1").emojioneArea();

        if($('#socket_id').val()==''){
            $('#mess').hide();
            $( ".inbox_people" ).after( "<div id='selUser' style='color:red'><h3>Please Select the User </h3></div>" );
        }

    })

    $('form').submit(function(e){

      e.preventDefault(); // prevents page reloading
      var url_string = window.location.href;
      var url = new URL(url_string);
      var c = url.searchParams.get("id");
      console.log($('.emoji_input img').attr('src'));
      if($('#m').val()!="" && $('#m').val()!= undefined ){
      socket.emit('createMeassage', {message:$('#m').val(),
        to:$('#socket_id').val(),
        reciver_id:$('#reciver_id').val(),
        from: c,
        createdAt:moment().format(' h:mm: a'), 
      }, function(data){
          console.log('got it',data);
      });
      $('.msg_history').append('<div class="outgoing_msg tempchat"><div class="sent_msg"><p>'+$('#m').val()+'</p><span class="time_date"> '+moment().format('h:mm a')+'   |  '+moment().format('MMM') +' ' + moment().date()+'  </span> </div></div>');
      $('#m').val('');
      scrollToBottom()
      return false;
    }
    });
    function scrollToBottom(){
        $('.msg_history').scrollTop($('.msg_history')[0].scrollHeight);
    }
  });

  
