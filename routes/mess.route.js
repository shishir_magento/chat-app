var express = require('express');
app = express();
var router = express.Router();
var messCtlr = require('../controllers/mess.controller');

router.route('/message')
  .post(messCtlr.getMessage);


module.exports = router;