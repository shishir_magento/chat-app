var conn = require('../config/db.connection');

function saveMessage(data){
    console.log(data);
    conn.query('INSERT INTO chat_messages SET ?', data, function(err, result) {
        if (err) throw err;
    });
    
}

function getUserForSender(senderId){
    return new Promise((resolve, reject) => {
        conn.query("select * from users where user_id in ( SELECT DISTINCT chat_receiver_id FROM `chat_messages` WHERE chat_sender_id = "+senderId+" and chat_group_id=0)", function (err, result, fields) {
            if (err) reject(err);
            return resolve(result);
        });
    }); 
}

function getChatDataOfParticularId(senderId, reciverId){
    return new Promise((resolve, reject) => {
        conn.query("SELECT * FROM `chat_messages` WHERE (chat_sender_id ="+ senderId+" and chat_receiver_id="+reciverId+") or (chat_sender_id = "+reciverId+" and chat_receiver_id="+senderId+") ORDER BY chat_id asc", function (err, result, fields) {
            if (err) reject(err);
            return resolve(result);
        });
    });
}

function getUserInfoById(Id){
    return new Promise((resolve, reject)=>{
        conn.query("SELECT * FROM `users` WHERE user_id ="+Id,function (err, result, fields) {
            if (err) reject(err);
            return resolve(result);
        });
    });
}

module.exports = {saveMessage, getUserForSender, getChatDataOfParticularId, getUserInfoById}

